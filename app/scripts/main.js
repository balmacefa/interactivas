function initMap() {
	var mapDiv = document.getElementById('map');
	var map = new google.maps.Map(mapDiv, {
		center: {
			lat: 9.981606099999999,
			lng: -84.73507010000003
		},
		zoom: 13
	});
}
$(document).ready(function() {

	var nav = $('#nav');
	$('#toogleMenu').click(function() {
		$(this).toggleClass('open');
		nav.toggleClass('open');
	});
});
